﻿using System;

namespace COMP5002
{
    class Program
    {
        static void Main(string[] args)
        {

            // print out a welcome message "Welcome to New World"
            Console.WriteLine("''''''''''''''''''''''''''''");
            Console.WriteLine("''''''''''''''''''''''''''''");
            Console.WriteLine("''''Welcome to NEW WORLD''''");
            Console.WriteLine("''''''''''''''''''''''''''''");
            Console.WriteLine("''''''''''''''''''''''''''''");
            
            // Declare Variables
            // guestName : String
            // firstItem : Double
            // guestAnswer : String
            // secondItem : Double
            // totalPrice : Double
            // priceGST : Double
            string guestName;
            double firstItem;
            string guestAnswer;
            double secondItem;
            double totalPrice = 0.00;
            double priceGST;
            double grandTotal = 0.00;

            // Print out a question "What is your name?"
            Console.WriteLine("What is your name?");

            // Read the person's name.
            // Place the string inside the guestName Variable
            guestName = Console.ReadLine();
            Console.WriteLine();

            // Print out a question "How much money is your first item?"
            Console.WriteLine("How much money is your first item?");

            // Read the price for the item.
            //Convert the string into a double
            // Place the double inside the firstItem Variable
            firstItem = double.Parse(Console.ReadLine());
            totalPrice = firstItem;
            Console.WriteLine();
            
            // Print out another question "Would you like another item?"
            // Yes or No question
            Console.WriteLine(" Would you like another item? Yes or No");

            // Read out the answer
            guestAnswer = Console.ReadLine();
            Console.WriteLine();

            // If questAnswer is Yes
            if(guestAnswer == "Yes" || guestAnswer == "y")
            {
                // Print out a message "How much money is your second item?"    
                Console.WriteLine("How much money is your second item?" );

                // Read the price for the second item
                // Place inside the secondItem Variable
                secondItem = double.Parse(Console.ReadLine());
                
                // Calculate total price. First item + second item.
                totalPrice = firstItem + secondItem;
                Console.WriteLine();

                // Print the value of totalPrice
                Console.WriteLine($"Your total price is {totalPrice}");
                Console.WriteLine();
            }
            //else. If questAnswer is No
            else if(guestAnswer == "No" || guestAnswer == "n")
            {
                Console.WriteLine();
                //Print out a message "Your total price today is"
                //Print the firstItem
                Console.WriteLine($"Your total price today is {totalPrice}");
            }

            // Calculate the GST by 15%
            priceGST = totalPrice *0.15;

            // Print the GST
            Console.WriteLine($"The GST will cost you an extra 15% {priceGST}");

            //Calculate the total price by the GST
            grandTotal  = totalPrice + priceGST;

            //Print the grandTotal
            Console.WriteLine($"Your grand total today is {grandTotal}");
            Console.WriteLine();

            //Finish message to the customer "Thank you for shopping with us, please come again".
            Console.WriteLine("Thank you for shopping with us, please come again");
        }
    }
}
